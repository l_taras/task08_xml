<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>List of banks</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Bank</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Profitability</th>
                    </tr>
                    <xsl:for-each select="banks/bank">
                        <xsl:sort select="name"/>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="amount"/></td>
                            <td><xsl:value-of select="profitability"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>