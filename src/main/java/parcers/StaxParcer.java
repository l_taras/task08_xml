package parcers;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import realisation.Bank;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParcer {
    public static void main(String[] args) throws Exception {
        File xmlFile = new File("src\\main\\resources\\XML\\bank.xml");
        StaxParser staxParser = new StaxParser();
        List<Bank> list = new ArrayList<>(staxParser.parse(xmlFile));
        for (Bank bank : list) {
            System.out.println(bank);
        }


    }

    private static class StaxParser {

        List<Bank> bankList = new ArrayList<>();
        Bank bank;

        public List<Bank> parse(File xml) throws Exception {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            try {
                XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
                while (xmlEventReader.hasNext()) {
                    XMLEvent xmlEvent = xmlEventReader.nextEvent();
                    if (xmlEvent.isStartElement()) {
                        StartElement startElement = xmlEvent.asStartElement();
                        String name = startElement.getName().getLocalPart();
                        switch (name) {
                            case "bank":
                                bank = new Bank();
                                break;
                            case "name":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setName(xmlEvent.asCharacters().getData());
                                break;
                            case "country":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setCountry(xmlEvent.asCharacters().getData());
                                break;
                            case "type":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setType(xmlEvent.asCharacters().getData());
                                break;
                            case "depositor":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setDepositor(xmlEvent.asCharacters().getData());
                                break;
                            case "accountId":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setAccountId(Integer.parseInt(xmlEvent.asCharacters().getData()));
                                break;
                            case "amount":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setAmount(Double.parseDouble(xmlEvent.asCharacters().getData()));
                                break;
                            case "profitability":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setProfitability(Integer.parseInt(xmlEvent.asCharacters().getData()));
                                break;
                            case "time":
                                xmlEvent = xmlEventReader.nextEvent();
                                bank.setTime(Integer.parseInt(xmlEvent.asCharacters().getData()));
                                break;

                        }
                    }
                    if (xmlEvent.isEndElement()) {
                        EndElement endElement = xmlEvent.asEndElement();
                        if (endElement.getName().getLocalPart().equals("bank")) {
                            bankList.add(bank);
                        }
                    }
                }
            } catch (FileNotFoundException | XMLStreamException e) {
                e.printStackTrace();
            }
            return bankList;
        }
    }
}


