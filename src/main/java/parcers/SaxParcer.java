package parcers;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import realisation.Bank;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;

public class SaxParcer {
    public static void main(String[] args) throws Exception {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser saxParser = spf.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        MyHandler handler = new MyHandler();
        xmlReader.setContentHandler(handler);
        xmlReader.parse("src\\main\\resources\\XML\\bank.xml");
        for (Bank bank : handler.getBankList()) {
            System.out.println(bank);
        }
    }

    private static class MyHandler extends DefaultHandler {

        static final String BANK_TAG = "bank";
        static final String NAME_TAG = "name";
        static final String COUNTRY_TAG = "country";
        static final String TYPE_TAG = "type";
        static final String DEPOSITOR_TAG = "depositor";
        static final String AMOUNT_TAG = "amount";
        static final String PROFITABILITY_TAG = "profitability";
        static final String TIME_TAG = "time";
        static final String ACCOUNTID_TAG = "accountId";

        private String currentElement;

        private List<Bank> listBanks = new ArrayList<>();
        private Bank bank;

        public List<Bank> getBankList() {
            return this.listBanks;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            currentElement = qName;

            switch (currentElement) {
                case BANK_TAG: {
                    bank = new Bank();
                    break;
                }


            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String text = new String(ch, start, length);

            if (text.contains("<") || currentElement == null) {
                return;
            }

            switch (currentElement) {
                case NAME_TAG: {
                    bank.setName(text);
                    break;
                }

                case COUNTRY_TAG: {
                    bank.setCountry(text);
                    break;
                }
                case TYPE_TAG: {
                    bank.setType(text);
                    break;
                }
                case DEPOSITOR_TAG: {
                    bank.setDepositor(text);
                    break;
                }
                case AMOUNT_TAG: {
                    bank.setAmount(Double.parseDouble(text));
                    break;
                }
                case PROFITABILITY_TAG: {
                    bank.setProfitability(Integer.parseInt(text));
                    break;
                }
                case TIME_TAG: {
                    bank.setTime(Integer.parseInt(text));
                    break;
                }
                case ACCOUNTID_TAG: {
                    bank.setAccountId(Integer.parseInt(text));
                    break;
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            switch (qName) {
                case BANK_TAG: {
                    listBanks.add(bank);
                    bank = null;
                }
                break;

            }

            currentElement = null;
        }

        public void endDocument() throws SAXException {
            System.out.println("XML parsing is completed.");
        }
    }
}


