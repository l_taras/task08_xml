package parcers;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import realisation.Bank;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DomParcer {
    public static void main(String[] args) throws Exception {
        File file = new File("src\\main\\resources\\XML\\bank.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);

        NodeList nodeBanks = document.getElementsByTagName("bank");
        List<Bank> listBanks = new ArrayList<>();

        for (int i = 0; i < nodeBanks.getLength(); i++) {
            if (nodeBanks.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element banksElement = (Element) nodeBanks.item(i);
                Bank bank = new Bank();
                NodeList childBanks = banksElement.getChildNodes();
                for (int j = 0; j < childBanks.getLength(); j++) {
                    if (childBanks.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        Element childElement = (Element) childBanks.item(j);
                        switch (childElement.getNodeName()) {
                            case "name": {
                                bank.setName(childElement.getTextContent());
                                break;
                            }
                            case "country": {
                                bank.setCountry(childElement.getTextContent());
                                break;
                            }
                            case "type": {
                                bank.setType(childElement.getTextContent());
                                break;
                            }
                            case "depositor": {
                                bank.setDepositor(childElement.getTextContent());
                                break;
                            }
                            case "accountId": {
                                bank.setAccountId(Integer.parseInt((childElement.getTextContent())));
                                break;
                            }
                            case "amount": {
                                bank.setAmount(Double.parseDouble((childElement.getTextContent())));
                                break;
                            }
                        }
                    }
                }
                listBanks.add(bank);
            }
        }
        for (Bank listBank : listBanks) {
            System.out.println(listBank);
        }
    }
}
